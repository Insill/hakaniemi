from django.shortcuts import render
from django.http import HttpResponse
from Hakaniemi import get_data
from Hakaniemi import create_table
# Create your views here.

def index(request):
    return render(
        request,
        "Hakaniemi/index.html",
        {
        })

def about(request):
    if(request.GET.get("button")):
        get_data.main()
        html = create_table.main()
    return render(
        request,
        "Hakaniemi/about.html",
        {
        })