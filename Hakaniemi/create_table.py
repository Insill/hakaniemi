import csv 
import logging
from datetime import date
from datetime import datetime
logger = logging.getLogger(__name__)

def main():
    filename = "data" + str(date.today()) + ".csv"
    html = '''<table>
    <tr>
        <th>Kuukausi</th>
        <th>Energiankulutus (kWh)</th>
    </tr>
    '''
    with open(filename,"rt") as file: 
        reader = csv.reader(file, delimiter=",")
        next(reader)
        for line in reader:
            if not line:
                continue
            else: 
                html += '''
    <tr>
        <td>{}</td>
        <td>{}</td> 
    </tr>
                '''.format(line[0],line[1])
        html += "</table>"

    logging.info("CSV File read")
    return html
