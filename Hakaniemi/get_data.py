import json
import requests
from datetime import date
from datetime import datetime
import csv 
import logging

logger = logging.getLogger(__name__)

def get_data():
    url = ('https://helsinki-openapi.nuuka.cloud/api/v1.0/EnergyData/Daily/'
    'ListByProperty?Record=LocationName&SearchString=1000%20Hakaniemen' 
    '%20kauppahalli&ReportingGroup=Electricity&StartTime=2019-01-01&EndTime=2019-12-31')

    response = requests.get(url)
    raw_data = json.loads(response.text)
    return raw_data

def calculate_kwh(raw_data):
    monthly_temp = {1:[], 2:[], 3:[], 4:[], 5:[], 6:[],
             7:[], 8:[], 9:[], 10:[], 11:[], 12:[]}
    monthly_final = {1:0, 2:0, 3:0, 4:0, 5:0 , 6:0 ,
                     7:0, 8:0, 9:0, 10:0, 11:0, 12:0}

    for item in raw_data:
        time_all = datetime.strptime(item["timestamp"],"%Y-%m-%dT%H:%M:%S")
        time_month = time_all.date().month
        monthly_temp[time_month].append(item["value"])

    for key,value in monthly_temp.items():
        monthly_final[key] = round(sum(value)/len(value),2)

    return monthly_final

def create_csv(monthly):
    filename = "data" + str(date.today()) + ".csv"
    labels = ["Month", "kWh"]

    with open(filename,"w",newline="") as file:
        try: 
            writer = csv.writer(file)
            writer.writerow(labels)
            writer.writerows(monthly.items())
        except IOError as e:
            logger.error("Error when trying to create CSV file: {}".format(e))

def main():
    raw_data = get_data()
    monthly = calculate_kwh(raw_data)
    create_csv(monthly)